CREATE TABLE `CommitValidationMessage` (
  `id` SERIAL,
  `commit_id` bigint(20) NOT NULL,
  `providerId` varchar(100) DEFAULT NULL,
  `authorEmail` varchar(100) DEFAULT NULL,
  `eclipseId` varchar(255) DEFAULT NULL,
  `statusCode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `CommitValidationStatus` (
  `id` SERIAL,
  `commitHash` varchar(100) NOT NULL,
  `project` varchar(100) NOT NULL,
  `lastModified` datetime DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `provider` varchar(100) NOT NULL,
  `repoUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `CommitValidationStatusGrouping` (
  `fingerprint` varchar(255) NOT NULL,
  `commit_id` bigint(20)  NOT NULL,
  PRIMARY KEY (`commit_id`,`fingerprint`)
);