/** ***************************************************************************** Copyright (C) 2020
 * Eclipse Foundation
 *
 * <p>This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * <p>SPDX-License-Identifier: EPL-2.0
 * *****************************************************************************/
package org.eclipsefoundation.git.eca.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.eclipsefoundation.git.eca.namespace.ProviderType;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.LowerCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Represents a request to validate a list of commits.
 *
 * @author Martin Lowe
 */
@AutoValue
@JsonNaming(LowerCamelCaseStrategy.class)
@JsonDeserialize(builder = AutoValue_ValidationRequest.Builder.class)
public abstract class ValidationRequest {
    @Nullable
    @JsonProperty("repoUrl")
    public abstract URI getRepoUrl();

    public abstract List<Commit> getCommits();

    public abstract ProviderType getProvider();

    @Nullable
    @JsonProperty("strictMode")
    public abstract Boolean getStrictMode();

    public static Builder builder() {
        return new AutoValue_ValidationRequest.Builder().setStrictMode(false).setCommits(new ArrayList<>());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        @JsonProperty("repoUrl")
        public abstract Builder setRepoUrl(@Nullable URI repoUrl);

        public abstract Builder setCommits(List<Commit> commits);

        public abstract Builder setProvider(ProviderType provider);

        @JsonProperty("strictMode")
        public abstract Builder setStrictMode(@Nullable Boolean strictMode);

        public abstract ValidationRequest build();
    }
}
