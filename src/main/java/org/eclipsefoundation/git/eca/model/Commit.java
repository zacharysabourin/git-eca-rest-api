/*******************************************************************************
 * Copyright (C) 2020 Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package org.eclipsefoundation.git.eca.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Represents a Git commit with basic data and metadata about the revision.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_Commit.Builder.class)
public abstract class Commit {
    @Nullable
	public abstract String getHash();

    @Nullable
	public abstract String getSubject();

    @Nullable
	public abstract String getBody();

    @Nullable
	public abstract List<String> getParents();

    @Nullable
	public abstract GitUser getAuthor();

    @Nullable
	public abstract GitUser getCommitter();

    @Nullable
	public abstract Boolean getHead();

	public static Builder builder() {
		return new AutoValue_Commit.Builder().setParents(new ArrayList<>());
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(withPrefix = "set")
	public abstract static class Builder {
		public abstract Builder setHash(@Nullable String hash);

		public abstract Builder setSubject(@Nullable String subject);

		public abstract Builder setBody(@Nullable String body);

		public abstract Builder setParents(@Nullable List<String> parents);

		public abstract Builder setAuthor(@Nullable GitUser author);

		public abstract Builder setCommitter(@Nullable GitUser committer);

		public abstract Builder setHead(@Nullable Boolean head);

		public abstract Commit build();
	}
}
