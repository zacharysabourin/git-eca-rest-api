package org.eclipsefoundation.git.eca.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.dto.CommitValidationMessage;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatusGrouping;
import org.eclipsefoundation.git.eca.helper.CommitHelper;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.CommitStatus;
import org.eclipsefoundation.git.eca.model.Project;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class DefaultValidationService implements ValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultValidationService.class);

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Override
    public List<CommitValidationStatus> getHistoricValidationStatus(RequestWrapper wrapper, String fingerprint) {
        if (StringUtils.isAllBlank(fingerprint)) {
            return Collections.emptyList();
        }
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.FINGERPRINT_RAW, fingerprint);
        RDBMSQuery<CommitValidationStatusGrouping> q = new RDBMSQuery<>(wrapper,
                filters.get(CommitValidationStatusGrouping.class), params);
        // set use limit to false to collect all data in one request
        q.setUseLimit(false);
        return dao.get(q).stream().map(statusGrouping -> statusGrouping.getCompositeId().getCommit())
                .collect(Collectors.toList());
    }

    @Override
    public List<CommitValidationStatus> getRequestCommitValidationStatus(RequestWrapper wrapper, ValidationRequest req,
            String projectId) {
        RDBMSQuery<CommitValidationStatus> q = new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatus.class),
                CommitHelper.getCommitParams(req, projectId));
        // set use limit to false to collect all data in one request
        q.setUseLimit(false);
        return dao.get(q);
    }

    @Override
    public void updateCommitValidationStatus(RequestWrapper wrapper, ValidationResponse r, ValidationRequest req,
            List<CommitValidationStatus> statuses, Project p) {
        // remove existing messaging
        RDBMSQuery<CommitValidationMessage> q = new RDBMSQuery<>(wrapper, filters.get(CommitValidationMessage.class),
                CommitHelper.getCommitParams(req, CommitHelper.getProjectId(p)));

        List<CommitValidationMessage> messages = new ArrayList<>();
        List<CommitValidationStatus> updatedStatuses = new ArrayList<>();
        for (Entry<String, CommitStatus> e : r.getCommits().entrySet()) {
            if (ValidationResponse.NIL_HASH_PLACEHOLDER.equalsIgnoreCase(e.getKey())) {
                LOGGER.warn("Not logging errors/validation associated with unknown commit");
                continue;
            }
            // update the status if present, otherwise make new one.
            Optional<CommitValidationStatus> status = statuses.stream()
                    .filter(s -> e.getKey().equals(s.getCommitHash())).findFirst();
            CommitValidationStatus base;
            if (status.isPresent()) {
                base = status.get();
            } else {
                base = new CommitValidationStatus();
                base.setProject(CommitHelper.getProjectId(p));
                base.setCommitHash(e.getKey());
                base.setProvider(req.getProvider());
                base.setRepoUrl(req.getRepoUrl().toString());
                base.setCreationDate(DateTimeHelper.now());
            }
            base.setLastModified(DateTimeHelper.now());
            updatedStatuses.add(base);
            // get the commit for current status
            Optional<Commit> commit = req.getCommits().stream().filter(c -> c.getHash().equals(e.getKey())).findFirst();
            if (commit.isEmpty()) {
                LOGGER.error("Could not find request commit associated with commit messages for commit hash '{}'",
                        e.getKey());
                continue;
            }
            Commit c = commit.get();
            // if there are errors, update validation messages
            if (e.getValue().getErrors().size() > 0 || (base.getErrors() != null && base.getErrors().size() > 0)) {
                // generate new errors, looking for errors not found in current list
                List<CommitValidationMessage> currentErrors = base.getErrors() != null ? base.getErrors()
                        : new ArrayList<>();
                List<CommitValidationMessage> newErrors = e.getValue().getErrors().stream().filter(
                        err -> currentErrors.stream().noneMatch(ce -> ce.getStatusCode() == err.getCode().getValue()))
                        .map(err -> {
                            CommitValidationMessage m = new CommitValidationMessage();
                            m.setAuthorEmail(c.getAuthor().getMail());
                            m.setStatusCode(err.getCode().getValue());
                            // TODO add a checked way to set this
                            m.setEclipseId(null);
                            m.setProviderId(null);
                            m.setCommit(base);
                            return m;
                        }).collect(Collectors.toList());
                LOGGER.debug("Encountered {} new errors for commit with hash '{}'", newErrors.size(), e.getKey());
                currentErrors.addAll(newErrors);
                // remove errors that weren't encountered on this run
                currentErrors.removeIf(err -> e.getValue().getErrors().isEmpty() || e.getValue().getErrors().stream()
                        .noneMatch(msg -> msg.getCode().getValue() == err.getStatusCode()));
                LOGGER.trace("Encountered {} errors: {}", currentErrors.size(), currentErrors);
                base.setErrors(currentErrors);
            }
        }
        String fingerprint = generateRequestHash(req);
        // update the base commit status and messages
        dao.add(new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatus.class)), updatedStatuses);
        dao.add(new RDBMSQuery<>(wrapper, filters.get(CommitValidationStatusGrouping.class)), updatedStatuses.stream()
                .map(s -> new CommitValidationStatusGrouping(fingerprint, s)).collect(Collectors.toList()));
    }
}
