package org.eclipsefoundation.git.eca.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.model.Project;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;

import io.undertow.util.HexConverter;

/**
 * 
 * @author martin
 *
 */
public interface ValidationService {

    /**
     * Retrieves a set of validation status objects given the validation request fingerprint.
     * 
     * @param wrapper current request wrapper object
     * @param fingerprint the validation request fingerprint
     * @return the list of historic validation status objects, or an empty list.
     */
    public List<CommitValidationStatus> getHistoricValidationStatus(RequestWrapper wrapper, String fingerprint);

    /**
     * Retrieves a set of commit validation status objects given a validation request and target project.
     * 
     * @param wrapper current request wrapper object
     * @param req the current validation request
     * @param projectId the project targeted by the validation request
     * @return the list of existing validation status objects for the validation request, or an empty list.
     */
    public List<CommitValidationStatus> getRequestCommitValidationStatus(RequestWrapper wrapper, ValidationRequest req,
            String projectId);

    /**
     * Updates or creates validation status objects for the commits validated as part of the current validation request.
     * Uses information from both the original request and the final response to generate details to be preserved in
     * commit status objects.
     * 
     * @param wrapper current request wrapper object
     * @param r the final validation response
     * @param req the current validation request
     * @param statuses list of existing commit validation objects to update
     * @param p the project targeted by the validation request.
     */
    public void updateCommitValidationStatus(RequestWrapper wrapper, ValidationResponse r, ValidationRequest req,
            List<CommitValidationStatus> statuses, Project p);


    /**
     * Generates a request fingerprint for looking up requests that have already been processed in the past. Collision
     * here is extremely unlikely, and low risk on the change it does. For that reason, a more secure but heavier
     * hashing alg. wasn't chosen.
     * 
     * @param req the request to generate a fingerprint for
     * @return the fingerprint for the request
     */
    default String generateRequestHash(ValidationRequest req) {
        StringBuilder sb = new StringBuilder();
        sb.append(req.getRepoUrl());
        req.getCommits().forEach(c -> sb.append(c.getHash()));
        try {
            return HexConverter.convertToHexString(MessageDigest.getInstance("MD5").digest(sb.toString().getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error while encoding request fingerprint - couldn't find MD5 algorithm.");
        }
    }
}
