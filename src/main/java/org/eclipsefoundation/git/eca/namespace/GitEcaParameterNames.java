package org.eclipsefoundation.git.eca.namespace;

import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public final class GitEcaParameterNames implements UrlParameterNamespace {
    public static final String COMMIT_ID_RAW = "commit_id";
    public static final String SHA_RAW = "sha";
    public static final String SHAS_RAW = "shas";
    public static final String PROJECT_ID_RAW = "project_id";
    public static final String REPO_URL_RAW = "repo_url";
    public static final String FINGERPRINT_RAW = "fingerprint";
    public static final UrlParameter COMMIT_ID = new UrlParameter(COMMIT_ID_RAW);
    public static final UrlParameter SHA = new UrlParameter(SHA_RAW);
    public static final UrlParameter SHAS = new UrlParameter(SHAS_RAW);
    public static final UrlParameter PROJECT_ID = new UrlParameter(PROJECT_ID_RAW);
    public static final UrlParameter REPO_URL = new UrlParameter(REPO_URL_RAW);
    public static final UrlParameter FINGERPRINT = new UrlParameter(FINGERPRINT_RAW);

    @Override
    public List<UrlParameter> getParameters() {
        return Arrays.asList(COMMIT_ID, SHA, SHAS, PROJECT_ID, REPO_URL);
    }

}
