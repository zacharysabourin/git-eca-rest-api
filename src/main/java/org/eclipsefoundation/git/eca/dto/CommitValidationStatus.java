package org.eclipsefoundation.git.eca.dto;

import java.time.ZonedDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Table
@Entity
public class CommitValidationStatus extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitValidationStatus.class, "cvs");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String commitHash;
    private String project;
    private String repoUrl;
    @Enumerated(EnumType.STRING)
    private ProviderType provider;
    private ZonedDateTime creationDate;
    private ZonedDateTime lastModified;
    @OneToMany(mappedBy = "commit",  fetch = FetchType.EAGER,cascade = { CascadeType.ALL }, orphanRemoval = true)
    private List<CommitValidationMessage> errors;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sha
     */
    public String getCommitHash() {
        return commitHash;
    }

    /**
     * @param commitHash the sha to set
     */
    public void setCommitHash(String commitHash) {
        this.commitHash = commitHash;
    }

    /**
     * @return the project
     */
    public String getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(String project) {
        this.project = project;
    }

    /**
     * @return the repoUrl
     */
    public String getRepoUrl() {
        return repoUrl;
    }

    /**
     * @param repoUrl the repoUrl to set
     */
    public void setRepoUrl(String repoUrl) {
        this.repoUrl = repoUrl;
    }

    /**
     * @return the provider
     */
    public ProviderType getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(ProviderType provider) {
        this.provider = provider;
    }

    /**
     * @return the creationDate
     */
    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the lastModified
     */
    public ZonedDateTime getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(ZonedDateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @return the messages
     */
    public List<CommitValidationMessage> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<CommitValidationMessage> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CommitValidationStatus [id=");
        builder.append(id);
        builder.append(", sha=");
        builder.append(commitHash);
        builder.append(", project=");
        builder.append(project);
        builder.append(", repoUrl=");
        builder.append(repoUrl);
        builder.append(", provider=");
        builder.append(provider);
        builder.append(", creationDate=");
        builder.append(creationDate);
        builder.append(", lastModified=");
        builder.append(lastModified);
        builder.append(", messages=");
        builder.append(errors.toString());
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class CommitValidationStatusFilter implements DtoFilter<CommitValidationStatus> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            // sha check
            String commitHash = params.getFirst(GitEcaParameterNames.SHA.getName());
            if (StringUtils.isNumeric(commitHash)) {
                stmt.addClause(
                        new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".commitHash = ?", new Object[] { commitHash }));
            }
            String projectId = params.getFirst(GitEcaParameterNames.PROJECT_ID.getName());
            if (StringUtils.isNumeric(projectId)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".projectId = ?",
                        new Object[] { projectId }));
            }
            List<String> commitHashes = params.get(GitEcaParameterNames.SHAS.getName());
            if (commitHashes != null && !commitHashes.isEmpty()) {
                stmt.addClause(
                        new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".commitHash IN ?", new Object[] { commitHashes }));
            }
            String repoUrl = params.getFirst(GitEcaParameterNames.REPO_URL.getName());
            if (StringUtils.isNotBlank(repoUrl)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".repoUrl = ?",
                        new Object[] { repoUrl }));
            }
            return stmt;
        }

        @Override
        public Class<CommitValidationStatus> getType() {
            return CommitValidationStatus.class;
        }
    }
}
