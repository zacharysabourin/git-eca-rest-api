package org.eclipsefoundation.git.eca.dto;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table
@Entity
public class CommitValidationMessage extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitValidationMessage.class, "cvm");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private CommitValidationStatus commit;
    private int statusCode;
    private String eclipseId;
    private String authorEmail;
    private String providerId;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the commit
     */
    @JsonIgnore
    public CommitValidationStatus getCommit() {
        return commit;
    }

    /**
     * @param commit the commit to set
     */
    @JsonIgnore
    public void setCommit(CommitValidationStatus commit) {
        this.commit = commit;
    }

    /**
     * @return the statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the providerId
     */
    public String getProviderId() {
        return providerId;
    }

    /**
     * @param providerId the providerId to set
     */
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
     * @return the eclipseId
     */
    public String getEclipseId() {
        return eclipseId;
    }

    /**
     * @param eclipseId the eclipseId to set
     */
    public void setEclipseId(String eclipseId) {
        this.eclipseId = eclipseId;
    }

    /**
     * @return the authorEmail
     */
    public String getAuthorEmail() {
        return authorEmail;
    }

    /**
     * @param authorEmail the authorEmail to set
     */
    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(authorEmail, commit, eclipseId, id, providerId, statusCode);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommitValidationMessage other = (CommitValidationMessage) obj;
        return Objects.equals(authorEmail, other.authorEmail) && Objects.equals(commit, other.commit)
                && Objects.equals(eclipseId, other.eclipseId) && Objects.equals(id, other.id)
                && Objects.equals(providerId, other.providerId) && statusCode == other.statusCode;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CommitValidationMessage [id=");
        builder.append(id);
        builder.append(", commit=");
        builder.append(commit.getCommitHash());
        builder.append(", statusCode=");
        builder.append(statusCode);
        builder.append(", eclipseId=");
        builder.append(eclipseId);
        builder.append(", authorEmail=");
        builder.append(authorEmail);
        builder.append(", providerId=");
        builder.append(providerId);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class CommitValidationMessageFilter implements DtoFilter<CommitValidationMessage> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // id check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                            new Object[] { Long.valueOf(id) }));
                }
                // commit id check
                String commitId = params.getFirst(GitEcaParameterNames.COMMIT_ID.getName());
                if (StringUtils.isNumeric(commitId)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".commitId = ?",
                            new Object[] { Integer.valueOf(commitId) }));
                }
                // ids check
                List<String> ids = params.get(DefaultUrlParameterNames.IDS.getName());
                if (ids != null && !ids.isEmpty()) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id IN ?",
                            new Object[] { ids.stream().filter(StringUtils::isNumeric).map(Long::valueOf)
                                    .collect(Collectors.toList()) }));
                }
            }
            return stmt;
        }

        @Override
        public Class<CommitValidationMessage> getType() {
            return CommitValidationMessage.class;
        }
    }
}
