package org.eclipsefoundation.git.eca.test.dao;

import javax.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;

import io.quarkus.test.Mock;

/**
 * Alternate mock DAO that ignores delete operations. Hibernate does updates initially rather than delete operations in
 * H2, which cause false negatives in tests
 * 
 * @author Martin Lowe
 *
 */
@Mock
@ApplicationScoped
public class TestPersistenceDao extends DefaultHibernateDao {

    @Override
    public <T extends BareNode> void delete(RDBMSQuery<T> q) {
        // TODO do proper deleting code, but this will not impact basic tests/functionality
    }

}
