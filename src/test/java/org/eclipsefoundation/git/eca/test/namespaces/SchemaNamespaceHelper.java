package org.eclipsefoundation.git.eca.test.namespaces;

public final class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";
    public static final String VALIDATION_REQUEST_SCHEMA_PATH = BASE_SCHEMAS_PATH + "validation-request"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String VALIDATION_RESPONSE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "validation-response"
            + BASE_SCHEMAS_PATH_SUFFIX;
}
