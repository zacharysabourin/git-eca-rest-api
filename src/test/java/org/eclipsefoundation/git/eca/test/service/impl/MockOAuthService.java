package org.eclipsefoundation.git.eca.test.service.impl;

import javax.inject.Singleton;

import org.eclipsefoundation.git.eca.service.OAuthService;

import io.quarkus.test.Mock;

@Mock
@Singleton
public class MockOAuthService implements OAuthService {

    @Override
    public String getToken() {
        return "";
    }

}
