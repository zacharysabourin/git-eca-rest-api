/*******************************************************************************
 * Copyright (C) 2020 Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.git.eca.api.ProjectsAPI;
import org.eclipsefoundation.git.eca.model.Project;
import org.eclipsefoundation.git.eca.model.Project.GitlabProject;
import org.eclipsefoundation.git.eca.model.Project.Repo;
import org.eclipsefoundation.git.eca.model.Project.User;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockProjectsAPI implements ProjectsAPI {

    private List<Project> src;

    @PostConstruct
    public void build() {
        this.src = new ArrayList<>();

        // sample repos
        Repo r1 = new Repo();
        r1.setUrl("http://www.github.com/eclipsefdn/sample");
        Repo r2 = new Repo();
        r2.setUrl("http://www.github.com/eclipsefdn/test");
        Repo r3 = new Repo();
        r3.setUrl("http://www.github.com/eclipsefdn/prototype.git");
        Repo r4 = new Repo();
        r4.setUrl("http://www.github.com/eclipsefdn/tck-proto");
        Repo r5 = new Repo();
        r5.setUrl("/gitroot/sample/gerrit.project.git");
        Repo r6 = new Repo();
        r6.setUrl("/gitroot/sample/gerrit.other-project");
        Repo r7 = new Repo();
        r7.setUrl("https://gitlab.eclipse.org/eclipse/dash/dash.git");
        Repo r8 = new Repo();
        r8.setUrl("https://gitlab.eclipse.org/eclipse/dash-second/dash.handbook.test");

        // sample users, correlates to users in Mock projects API
        User u1 = User.builder().setUrl("").setUsername("da_wizz").build();
        User u2 = User.builder().setUrl("").setUsername("grunter").build();

        
        // projects
        Project p1 = Project.builder().setName("Sample project").setProjectId("sample.proj")
                .setSpecProjectWorkingGroup(Collections.emptyList()).setGithubRepos(Arrays.asList(r1, r2))
                .setGerritRepos(Arrays.asList(r5)).setCommitters(Arrays.asList(u1, u2)).setGitlab(
                        GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList()).setProjectGroup("").build()).build();
        src.add(p1);

        Project p2 = Project.builder().setName("Prototype thing").setProjectId("sample.proto")
                .setSpecProjectWorkingGroup(Collections.emptyList()).setGithubRepos(Arrays.asList(r3))
                .setGerritRepos(Arrays.asList(r6)).setGitlabRepos(Arrays.asList(r8)).setCommitters(Arrays.asList(u2)).setGitlab(
                        GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList()).setProjectGroup("eclipse/dash-second").build())
                .build();
        src.add(p2);

        Map<String, String> map = new HashMap<>();
        map.put("id", "proj1");
        Project p3 = Project.builder().setName("Spec project").setProjectId("spec.proj").setSpecProjectWorkingGroup(map)
                .setGithubRepos(Arrays.asList(r4)).setGitlabRepos(Arrays.asList(r7)).setGitlab(
                        GitlabProject.builder().setIgnoredSubGroups(Arrays.asList("eclipse/dash/mirror")).setProjectGroup("eclipse/dash").build())
                .setCommitters(Arrays.asList(u1, u2)).build();
        src.add(p3);
    }

    @Override
    public List<Project> getProject(int page, int pageSize) {
        return page == 1 ? new ArrayList<>(src) : Collections.emptyList();
    }
}
