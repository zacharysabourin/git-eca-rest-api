/**
 * Copyright (C) 2020 Eclipse Foundation
 *
 * <p>This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * <p>SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.test.namespaces.SchemaNamespaceHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

/**
 * Tests for verifying end to end validation via the endpoint. Uses restassured to create pseudo requests, and Mock API
 * endpoints to ensure that all data is kept internal for test checks.
 *
 * @author Martin Lowe
 */
@QuarkusTest
class ValidationResourceTest {
    public static final String ECA_BASE_URL = "/eca";

    @Inject
    CachingService cs;
    @Inject
    ObjectMapper json;

    @BeforeEach
    void cacheClear() {
        // if dev servers are run on the same machine, some values may live in the cache
        cs.removeAll();
    }

    @Test
    void validate() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();

        // test output w/ assertions
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validate_success_format() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(Arrays.asList(c1)).build();
        given().when().body(vr).contentType(ContentType.JSON).post(ECA_BASE_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.VALIDATION_RESPONSE_SCHEMA_PATH));
    }

    @Test
    void validate_success_inputFormat() throws URISyntaxException {
        // Check that the input matches what is specified in spec
        // set up test users
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(Arrays.asList(c1)).build();
        // convert the object to JSON
        String in;
        try {
            in = json.writeValueAsString(vr);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.VALIDATION_REQUEST_SCHEMA_PATH).matches(in));
        // known good request
        given().contentType(ContentType.JSON).body(vr).when().post(ECA_BASE_URL).then()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.VALIDATION_RESPONSE_SCHEMA_PATH))
                .statusCode(200);
    }

    @Test
    void validateMultipleCommits() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        GitUser g2 = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        Commit c2 = Commit.builder().setAuthor(g2).setCommitter(g2)
                .setBody("Signed-off-by: Grunts McGee<grunt@important.co>")
                .setHash("c044dca1847c94e709601651339f88a5c82e3cc7").setSubject("Add in feature")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c2);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();

        // test output w/ assertions
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateMergeCommit() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Rando Calressian").setMail("rando@nowhere.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things").setParents(Arrays
                        .asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10", "46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c11"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // No errors expected, should pass as only commit is a valid merge commit
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateCommitNoSignOffCommitter() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setBody("").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/prototype")).setCommits(commits).build();

        // test output w/ assertions
        // Should be valid as Grunt is a committer on the prototype project
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateCommitNoSignOffNonCommitter() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setBody("").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/prototype.git")).setCommits(commits).build();

        // test output w/ assertions
        // Should be valid as wizard has signed ECA
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateCommitInvalidSignOff() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Barshall Blathers").setMail("slom@eclipse-foundation.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), "barshallb@personal.co"))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/prototype.git")).setCommits(commits).build();

        // test output w/ assertions
        // Should be valid as signed off by footer is no longer checked
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateCommitSignOffMultipleFooterLines_Last() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Barshall Blathers").setMail("slom@eclipse-foundation.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Change-Id: 0000000000000001\nSigned-off-by: %s <%s>", g1.getName(),
                        g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/prototype")).setCommits(commits).build();

        // test output w/ assertions
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateCommitSignOffMultipleFooterLines_First() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Barshall Blathers").setMail("slom@eclipse-foundation.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>\nChange-Id: 0000000000000001", g1.getName(),
                        g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/prototype")).setCommits(commits).build();

        // test output w/ assertions
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateCommitSignOffMultipleFooterLines_Multiple() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Barshall Blathers").setMail("slom@eclipse-foundation.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Change-Id: 0000000000000001\\nSigned-off-by: %s <%s>\nSigned-off-by: %s <%s>",
                        g1.getName(), g1.getMail(), g1.getName(), "barshallb@personal.co"))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/prototype")).setCommits(commits).build();

        // test output w/ assertions
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));
    }

    @Test
    void validateWorkingGroupSpecAccess() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        GitUser g2 = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();

        // CASE 1: WG Spec project write access valid
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/tck-proto")).setCommits(commits).build();

        // test output w/ assertions
        // Should be valid as Wizard has spec project write access + is committer
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));

        // CASE 2: No WG Spec proj write access
        commits = new ArrayList<>();
        // create sample commits
        c1 = Commit.builder().setAuthor(g2).setCommitter(g2)
                .setBody(String.format("Signed-off-by: %s <%s>", g2.getName(), g2.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);
        vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/tck-proto")).setCommits(commits).build();

        // test output w/ assertions
        // Should be invalid as Grunt does not have spec project write access
        // Should have 2 errors, as both users get validated

        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(2), "commits." + c1.getHash() + ".errors[0].code",
                is(APIStatusCode.ERROR_SPEC_PROJECT.getValue()));
    }

    @Test
    void validateNoECA_author() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();

        GitUser g2 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g2)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Error should be singular + that there's no ECA on file
        // Status 403 (forbidden) is the standard return for invalid requests
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1));
    }

    @Test
    void validateNoECA_committer() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();

        GitUser g2 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g2).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g2.getName(), g2.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Error count should be 1 for just the committer access
        // Status 403 (forbidden) is the standard return for invalid requests
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1));
    }

    @Test
    void validateNoECA_both() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Should have 2 errors, 1 for author entry and 1 for committer entry
        // Status 403 (forbidden) is the standard return for invalid requests
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(2));
    }

    @Test
    void validateAuthorNoEclipseAccount() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Rando Calressian").setMail("rando@nowhere.co").build();

        GitUser g2 = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g2)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Error should be singular + that there's no Eclipse Account on file for author
        // Status 403 (forbidden) is the standard return for invalid requests
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1));
    }

    @Test
    void validateCommitterNoEclipseAccount() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Rando Calressian").setMail("rando@nowhere.co").build();

        GitUser g2 = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g2).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g2.getName(), g2.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Error should be singular + that there's no Eclipse Account on file for committer
        // Status 403 (forbidden) is the standard return for invalid requests
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1));
    }

    @Test
    void validateProxyCommitUntrackedProject() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Rando Calressian").setMail("rando@nowhere.co").build();

        GitUser g2 = GitUser.builder().setName("Grunts McGee").setMail("grunt@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g2).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g2.getName(), g2.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample-not-tracked")).setCommits(commits).build();
        // test output w/ assertions
        // Should be valid as project is not tracked
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGithub() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("projbot").setMail("1.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Should be valid as bots should only commit on their own projects (including aliases)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGithub_untracked() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("projbot").setMail("1.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample-untracked")).setCommits(commits).build();
        // test output w/ assertions
        // Should be valid as bots can commit on any untracked project (legacy support)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGithub_invalidBot() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Should be invalid as bots should only commit on their own projects (including aliases)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);
    }

    @Test
    void validateBotCommiterAccessGithub_wrongEmail() throws URISyntaxException {
        // set up test users - uses Gerrit/LDAP email (wrong for case)
        GitUser g1 = GitUser.builder().setName("protobot").setMail("2.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(new URI("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();
        // test output w/ assertions
        // Should be invalid as wrong email was used for bot (uses Gerrit bot email)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);
    }

    @Test
    void validateBotCommiterAccessGitlab() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITLAB)
                .setRepoUrl(new URI("https://gitlab.eclipse.org/eclipse/dash-second/dash.handbook.test")).setCommits(commits)
                .build();
        // test output w/ assertions
        // Should be valid as bots should only commit on their own projects (including aliases)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGitlab_ignored() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITLAB)
                .setRepoUrl(new URI("https://gitlab.eclipse.org/eclipse/dash/mirror/dash.handbook.untracked"))
                .setCommits(commits).build();
        // test output w/ assertions
        // Should be valid as bots can commit on any untracked project (legacy support)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGitlab_invalidBot() throws URISyntaxException {
        // set up test users (wrong bot for project)
        GitUser g1 = GitUser.builder().setName("specbot").setMail("3.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITLAB)
                .setRepoUrl(new URI("https://gitlab.eclipse.org/eclipse/dash-second/dash.handbook.test")).setCommits(commits)
                .build();
        // test output w/ assertions
        // Should be invalid as bots should only commit on their own projects
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);
    }

    @Test
    void validateBotCommiterAccessGitlab_wrongEmail() throws URISyntaxException {
        // set up test users - uses Gerrit/LDAP email (expects Gitlab email)
        GitUser g1 = GitUser.builder().setName("specbot").setMail("3.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITLAB)
                .setRepoUrl(new URI("https://gitlab.eclipse.org/eclipse/dash/dash.git")).setCommits(commits).build();
        // test output w/ assertions
        // Should be valid as wrong email was used, but is still bot email alias (uses Gerrit bot email)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGerrit() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("protobot").setMail("2.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as bots should only commit on their own projects (including aliases)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGerrit_untracked() throws URISyntaxException {
        // set up test users
        GitUser g1 = GitUser.builder().setName("protobot").setMail("2.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/untracked.project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as bots can commit on any untracked project (legacy support)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateBotCommiterAccessGerrit_invalidBot() throws URISyntaxException {
        // set up test users - (wrong bot for project)
        GitUser g1 = GitUser.builder().setName("specbot").setMail("3.bot@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be invalid as bots should only commit on their own projects (wrong project)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);

    }

    @Test
    void validateBotCommiterAccessGerrit_aliasEmail() throws URISyntaxException {
        // set up test users - uses GH (instead of expected Gerrit/LDAP email)
        GitUser g1 = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as wrong email was used, but is still bot email alias
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateNullEmailCheck() throws URISyntaxException {
        // set up test users - uses GH (instead of expected Gerrit/LDAP email)
        GitUser g1 = GitUser.builder().setName("protobot-gh").setMail("2.bot-github@eclipse.org").build();
        GitUser g2 = GitUser.builder().setName("protobot-gh").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g2).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be invalid as there is no email (refuse commit, not server error)
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);
    }

    @Test
    void validateGithubNoReply_legacy() throws URISyntaxException {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunter@users.noreply.github.com").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateGithubNoReply_success() throws URISyntaxException {
        // sometimes the user ID and user name are reversed
        GitUser g1 = GitUser.builder().setName("grunter").setMail("123456789+grunter@users.noreply.github.com").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateGithubNoReply_nomatch() throws URISyntaxException {
        GitUser g1 = GitUser.builder().setName("some_guy").setMail("123456789+some_guy@users.noreply.github.com")
                .build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be invalid as no user exists with "Github" handle that matches some_guy
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);
    }

    @Test
    void validateGithubNoReply_nomatch_legacy() throws URISyntaxException {
        GitUser g1 = GitUser.builder().setName("some_guy").setMail("some_guy@users.noreply.github.com").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be invalid as no user exists with "Github" handle that matches some_guy
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403);
    }

    @Test
    void validateAllowListAuthor_success() throws URISyntaxException {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunter@users.noreply.github.com").build();
        GitUser g2 = GitUser.builder().setName("grunter").setMail("noreply@github.com").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g2).setCommitter(g1).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    @Test
    void validateAllowListCommitter_success() throws URISyntaxException {
        GitUser g1 = GitUser.builder().setName("grunter").setMail("grunter@users.noreply.github.com").build();
        GitUser g2 = GitUser.builder().setName("grunter").setMail("noreply@github.com").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g2).setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setProvider(ProviderType.GERRIT)
                .setRepoUrl(new URI("/gitroot/sample/gerrit.other-project")).setCommits(commits).setStrictMode(true)
                .build();
        // test output w/ assertions
        // Should be valid as grunter used a no-reply Github account and has a matching GH handle
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200);
    }

    /*
     * 
     * DB PERSISTENCE TESTS
     * 
     */

    @Test
    void validateRevalidation_success() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();

        // test output w/ assertions
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0));

        // repeat call to test that skipped status is passed
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0), "commits." + c1.getHash() + ".messages[0].code",
                is(APIStatusCode.SUCCESS_SKIPPED.getValue()));
    }

    @Test
    void validateRevalidation_errors() {
        // set up test users
        GitUser g1 = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();
        GitUser g2 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();

        // create sample commits
        List<Commit> commits = new ArrayList<>();
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g2)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c1);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();

        // test output w/ assertions
        // should fail with 1 error
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1));

        // repeat call to test that previously run check still fails
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1), "commits." + c1.getHash() + ".errors[0].code",
                is(APIStatusCode.ERROR_AUTHOR.getValue()));
    }

    @Test
    void validateRevalidation_partialSuccess() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        GitUser g2 = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();

        List<Commit> commits = new ArrayList<>();
        // create sample commits
        // successful commit
        Commit c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>").setHash(UUID.randomUUID().toString())
                .setSubject("All of the things").setParents(Collections.emptyList()).build();
        commits.add(c1);
        // error commit
        Commit c2 = Commit.builder().setAuthor(g2).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        commits.add(c2);

        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(commits).build();

        // test output w/ assertions
        // should fail with 1 error
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1));

        // repeat call to test that previously run check still fails
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "commits." + c2.getHash() + ".errors[0].code", is(APIStatusCode.ERROR_AUTHOR.getValue()),
                "commits." + c1.getHash() + ".messages[0].code", is(APIStatusCode.SUCCESS_SKIPPED.getValue()));
    }

    @Test
    void validateRevalidation_commitUpdatedAfterError() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        GitUser g2 = GitUser.builder().setName("Newbie Anon").setMail("newbie@important.co").build();

        // create sample commits
        Commit c1 = Commit.builder().setAuthor(g2).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail()))
                .setHash(UUID.randomUUID().toString()).setSubject("All of the things")
                .setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10")).build();
        ValidationRequest vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(Arrays.asList(c1))
                .build();

        // test output w/ assertions
        // should fail with 1 error
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(403).body("passed",
                is(false), "errorCount", is(1), "commits." + c1.getHash() + ".errors[0].code",
                is(APIStatusCode.ERROR_AUTHOR.getValue()));

        // simulate fixed ECA by updating author and using same hash
        c1 = Commit.builder().setAuthor(g1).setCommitter(g1)
                .setBody(String.format("Signed-off-by: %s <%s>", g1.getName(), g1.getMail())).setHash(c1.getHash())
                .setSubject("All of the things").setParents(Arrays.asList("46bb69bf6aa4ed26b2bf8c322ae05bef0bcc5c10"))
                .build();
        vr = ValidationRequest.builder().setStrictMode(false).setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample")).setCommits(Arrays.asList(c1))
                .build();

        // repeat call to test that previously run check now passes
        // Check that message code is not skipped
        given().body(vr).contentType(ContentType.JSON).when().post(ECA_BASE_URL).then().statusCode(200).body("passed",
                is(true), "errorCount", is(0), "commits." + c1.getHash() + ".messages[0].code",
                not(APIStatusCode.SUCCESS_SKIPPED.getValue()));
    }
}
