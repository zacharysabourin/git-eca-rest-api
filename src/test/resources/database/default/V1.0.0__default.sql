CREATE TABLE CommitValidationStatus (
  commitHash varchar(100) NOT NULL,
  project varchar(100) DEFAULT NULL,
  lastModified datetime DEFAULT NULL,
  creationDate datetime DEFAULT NULL,
  id bigint(20) NOT NULL AUTO_INCREMENT,
  provider varchar(100) NOT NULL,
  repoUrl varchar(255) NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO CommitValidationStatus(id,commitHash,project,lastModified,creationDate,provider, repoUrl) VALUES(1,'123456789', 'sample.proj', NOW(), NOW(),'GITHUB','http://www.github.com/eclipsefdn/sample');
INSERT INTO CommitValidationStatus(id,commitHash,project,lastModified,creationDate,provider, repoUrl) VALUES(2,'123456789', 'sample.proto', NOW(), NOW(),'GITLAB','');
INSERT INTO CommitValidationStatus(id,commitHash,project,lastModified,creationDate,provider, repoUrl) VALUES(3,'987654321', 'sample.proto', NOW(), NOW(),'GITLAB','');
INSERT INTO CommitValidationStatus(id,commitHash,lastModified,creationDate,provider, repoUrl) VALUES(5,'abc123def456', NOW(), NOW(),'GITHUB','http://www.github.com/eclipsefdn/sample');

CREATE TABLE CommitValidationMessage (
  providerId varchar(100) DEFAULT NULL,
  authorEmail varchar(100) DEFAULT NULL,
  eclipseId varchar(255) DEFAULT NULL,
  id bigint(20) NOT NULL AUTO_INCREMENT,
  commit_id int(11) NOT NULL,
  statusCode int(11) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO CommitValidationMessage(id,commit_id,providerId,authorEmail,eclipseId,statusCode) VALUES(4,1,'','','',-405);

CREATE TABLE `CommitValidationStatusGrouping` (
  `fingerprint` varchar(255) NOT NULL,
  `commit_id` bigint(20) NOT NULL,
  PRIMARY KEY (`commit_id`,`fingerprint`)
);
INSERT INTO CommitValidationStatusGrouping(fingerprint, commit_id) VALUES('957706b0f31e0ccfc5287c0ebc62dc79', 1);